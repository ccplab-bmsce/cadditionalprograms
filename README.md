**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

Class codes for additional programs for each section.

Rules for Additional Programs.
Commit Rules
Write "Initial" Commit before Compilation.
Write "PeerHelp" commit if your seeking help from a peer.
Write "InstructorHelp" Commit if your seeking help from an instructor.
Write "Improvement" Commits for changes/improvements made by you.
Write "Copied" Commits if you copied code from peer or internet or text book.
Write "PeerReviewed" commits if you made changes suggested by peers
Write "InstructorReviewed" commits if you made changes suggested by the instructor.

Update Corresponding txt file.
1. With Questions and Answers with references ( Links from google, pageno from the textbooks)
2. With Experiments

How to help a peer.

Check if the peer has committed PeerHelp commit in bitbucket.
Check if the peer has written a question in the txt file.
Ask questions and do not answer.

## Goals of the Class

1. Overcome **Completion Anxiety**,**Fear**,**Shame**,**Anger**,**Sadness**.
2. Achieve **Flow** 
3. Achieve **Mindfulness** 
[Mindfulness and Flow (https://www.quora.com/What-is-the-difference-between-mindfulness-and-flow)]


